<?php
/**
 * Created by PhpStorm.
 * User: krizmanicsnorbert
 * Date: 2018. 11. 09.
 * Time: 10:24
 */

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;




class Admin extends Authenticatable
{
    /**
     * Admin constructor.
     */

    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','job_title',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

