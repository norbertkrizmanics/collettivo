<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	// Authentication Routes
	Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);


// Admin Routes
    Route::group(['prefix'=>'admin'], function (){
        Route::get('/login',['uses'=>'Auth\AdminLoginController@showLoginForm','as'=>'admin.login']);
        Route::post('/login',['uses'=>'Auth\AdminLoginController@login','as'=>'admin.login.submit']);
        Route::post('/logout',['uses'=>'Auth\AdminController@logout','as'=>'admin.logout']);
        Route::get('/',['uses'=>'Auth\AdminController@index','as'=>'admin.dashboard']);
    });



    // Password Reset Routes
	Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
	Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
	Route::post('password/reset', 'Auth\PasswordController@reset');

	// Categories
	Route::resource('categories', 'CategoryController', ['except' => ['create']]);
	Route::resource('tags', 'TagController', ['except' => ['create']]);
	
	// Comments
	Route::post('comments/{post_id}', ['uses' => 'CommentsController@store', 'as' => 'comments.store']);
	Route::get('comments/{id}/edit', ['uses' => 'CommentsController@edit', 'as' => 'comments.edit']);
	Route::put('comments/{id}', ['uses' => 'CommentsController@update', 'as' => 'comments.update']);
	Route::delete('comments/{id}', ['uses' => 'CommentsController@destroy', 'as' => 'comments.destroy']);
	Route::get('comments/{id}/delete', ['uses' => 'CommentsController@delete', 'as' => 'comments.delete']);


	Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
	Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
    Route::get('contact', ['uses'=>'PagesController@getContact','as'=>'contact']);
    Route::post('contact', 'PagesController@postContact');
    Route::get('marketing',['uses'=>'PagesController@getMarketing','as'=>'marketing']);
	Route::get('webfejlesztes', ['uses'=>'PagesController@getIt','as'=>'webfejlesztes']);
	Route::get('portfolio',['uses'=>'PagesController@getPortfolio','as'=>'portfolio']);
	Route::get('price',['uses'=>'PagesController@getPrice','as'=>'price']);
	Route::get('gdpr',['uses'=>'PagesController@getGdpr','as'=>'gdpr']);
	Route::get('/', 'PagesController@getIndex');
	Route::resource('posts', 'PostController');
});

Route::group(['middleware' => ['auth']], function () {

// Registration Routes
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');


});

