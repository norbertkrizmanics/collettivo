<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use Mail;
use Session;

class PagesController extends Controller {

	public function getIndex() {
		$posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
		return view('pages.welcome')->withPosts($posts);
	}

	public function getIt() {

		return view('pages.webfejlesztes');
	}
    public function getMarketing(){
	    return view('pages.marketing');
    }
    public function getPortfolio(){
	    return view('pages.portfolio');
    }
	public function getContact() {
		return view('pages.contact');
	}
	public function getPrice(){
	    return view('pages.price');
    }
    public function getGdpr(){
	    return view('pages.gdpr');
    }
	public function postContact(Request $request) {
		$this->validate($request, [
		    'name'=>'required',
			'email' => 'required|email',
			'subject' => 'min:3',
			'message' => 'min:10']);

		$data = array(
		    'name'=>$request->name,
			'email' => $request->email,
			'subject' => $request->subject,
			'bodyMessage' => $request->message
			);
		$emails = [
		    'krizmanics.norbert@gmail.com',
            'radaibarbara@gmail.com'
        ];

		Mail::send('emails.contact', $data, function($message) use ($data,$emails){
			$message->from($data['email']);
			$message->to($emails);
			$message->subject($data['subject']);
		});

		Session::flash('success', 'Köszönjük üzenetét! Munkatársaink hamarosan felveszik Önnel a kapcsolatot.');

		return redirect('/');
	}


}