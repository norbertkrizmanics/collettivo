<?php
/**
 * Created by PhpStorm.
 * User: krizmanicsnorbert
 * Date: 2018. 11. 09.
 * Time: 10:52
 */

namespace App\Http\Controllers\Auth;
use App\Admin;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

use App\Http\Controllers\Controller;


class AdminController extends Controller
{
/*
 * create a new contoller instance
 * @return void
 * */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /*
     * show the application dashboard
     * @return \Illuminate\Http\Response
     * */

    public function index(){
        return view('admin');
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        return redirect()->guest(route( 'admin.login' ));
    }
}