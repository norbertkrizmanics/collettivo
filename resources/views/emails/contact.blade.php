<h3>You Have a New Contact Via the Contact Form</h3>
<h4>From {{$name}}</h4>
<p>Sent via {{ $email }}</p>
<hr>
<div>
	{{ $bodyMessage }}
</div>

