
<hr>

      <!-- Footer -->
      <section id="footer">
            <div class="container">

                  <div class="row">
                        <div class="col-md-12 text-center">
                              <p><a class="text-green ml-2"style="color: #57a544;" href="/gdpr">GDPR</a></p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                              <ul class="list-unstyled list-inline social text-center">
                                    <li class="list-inline-item"><a href="https://www.facebook.com/collettivohungary/" target="_blank"><i class="fa fa-facebook"style="color: #57a544;"></i></a></li>
                                    <li class="list-inline-item"><a href="https://www.instagram.com/collettivo_hungary/" target="_blank"><i class="fa fa-instagram"style="color: #57a544;"></i></a></li>
                                    <li class="list-inline-item"><a href="/contact" target="_blank"><i class="fa fa-envelope"style="color: #57a544;"></i></a></li>
                              </ul>
                        </div>
                        </hr>
                  </div>
                  <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                              <p class="h5">&copy Minden jog fenntartva. | 2018 | <a class="text-green ml-2"style="color: #57a544;" href="/">COLLETTIVO</a></p>
                        </div>
                        </hr>
                  </div>
            </div>
      </section>

      <!-- ./Footer -->