<!-- Default Bootstrap Navbar -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">COLLETTIVO</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav ">
        <li class="{{ Request::is('/') ? "active" : "" }}"><a href="/">Home</a></li>
        <li class="{{ Request::is('marketing') ? "active" : "" }}"><a href="/marketing">Marketing</a></li>
        <li class="{{ Request::is('blog') ? "active" : "" }}"><a href="/blog">Blog</a></li>
        <li class="{{ Request::is('webfejlesztes') ? "active" : "" }}"><a href="/webfejlesztes">Webfejlesztés</a></li>
        <li class="{{ Request::is('portfolio') ? "active" : "" }}"><a href="/portfolio">Portfolio</a></li>
        <li class="{{ Request::is('price') ? "active" : "" }}"><a href="/price">Áraink</a></li>
        <li class="{{ Request::is('contact') ? "active" : "" }}"><a href="/contact">Elérhetőség</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::check())
        
        <li class="dropdown">
          <a href="/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello {{ Auth::user()->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('posts.index') }}">Bejegyzések</a></li>
            <li><a href="{{ route('categories.index') }}">Kategóriák</a></li>
            <li><a href="{{ route('tags.index') }}">Címkék</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ route('logout') }}">Kijelentkezés</a></li>
          </ul>
        </li>
        
        @else
        
          <a href="{{ route('login') }}" class="btn btn-default btn-xs btn-login ">Bejelentkezés</a>

        @endif

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>