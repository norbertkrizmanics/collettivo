@extends('main')

@section('title', "| Edit Tag")
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')
	
	{{ Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => "PUT"]) }}
		
		{{ Form::label('name', "Title:") }}
		{{ Form::text('name', null, ['class' => 'form-control']) }}

		{{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
	{{ Form::close() }}

@endsection