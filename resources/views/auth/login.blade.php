@extends('main')

@section('title', '| Bejelentkezés')
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')
	
	<div class="row">
		<div class="col-md-5 col-md-offset-4 ">
			{!! Form::open() !!}

				{{ Form::label('email', 'Email:') }}
				{{ Form::email('email', null, ['class' => 'form-control']) }}

				{{ Form::label('password', "Jelszó:") }}
				{{ Form::password('password', ['class' => 'form-control']) }}
				
				<br>
				{{ Form::checkbox('remember') }}{{ Form::label('remember', "Emlékezz rám") }}
				
				<br>
				{{ Form::submit('Bejelentkezés', ['class' => 'btn btn-success btn-block']) }}

				<p><a href="{{ url('password/reset') }}">Elfelejtettem a jelszavam</a>


			{!! Form::close() !!}
		</div>
	</div>


@endsection