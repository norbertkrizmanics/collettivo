@extends('main')

@section('title', "| Edit Category")
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')

    {{ Form::model($category, ['route' => ['categories.update', $category->id], 'method' => "PUT"]) }}

    {{ Form::label('name', "Title:") }}
    {{ Form::text('name', null, ['class' => 'form-control']) }}

    {{ Form::submit('Save Changes', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
    {{ Form::close() }}

@endsection