@extends('main')

@section('title', '| All Categories')
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')

	<div class="row">
		<div class="col-md-8">
			<h1>Categories</h1>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Edit</th>
						<th>Delete</th>

					</tr>
				</thead>

				<tbody>
					@foreach ($categories as $category)
					<tr>
						<th>{{ $category->id }}</th>
						<td><a href="{{ route('categories.show', $category->id) }}">{{ $category->name }}</a></td>
						<td><a href="{{ route('categories.show', $category->id) }}" class="btn btn-primary">Edit</a></td>
						<td><a href="{{ route('categories.destroy', $category->id) }}" class="btn btn-danger">Delete</a></td>

					</tr>
					@endforeach
				</tbody>
			</table>
		</div> <!-- end of .col-md-8 -->

		<div class="col-md-3">
			<div class="well">
				{!! Form::open(['route' => 'categories.store', 'method' => 'POST']) !!}
					<h2>New Category</h2>
					{{ Form::label('name', 'Name:') }}
					{{ Form::text('name', null, ['class' => 'form-control']) }}

					{{ Form::submit('Create New Category', ['class' => 'btn btn-primary btn-block btn-h1-spacing']) }}
				
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@endsection