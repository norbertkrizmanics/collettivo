@extends('main')
@section('title','| Áraink')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 text-center">
            <h1>
                <a href="" class="typewrite section-title h1" data-period="2000" data-type='[ "ÁRAINK" ]'>
                    <span class="wrap"></span>
                </a>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <!-- Image Section - set the background image for the header in the line below -->
                <section class="py-5 bg-image-full img-responsive how-img" style="background-image: url('/images/buy2.jpg');" >
                    <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
                    <div style="height: 200px;">

                    </div>

                </section>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">

            <section id="prices">
                <div class="container">

                    <div class="row">

                        <div class="col-lg-6 col-lg-offset-3 text-center">
                            <h2 class="section-title"><span class=" ion-minus"></span>Egyedi kedvező SOCIAL árak!<span class="ion-minus"></span></h2>
                            <p>Minden projekt más. Ezért az egyedi árakban hiszünk. </p><br>
                        </div>
                    </div>


                    <div class="prices-box">
                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-right">

                                <div class="top-content text-center">

                                    <div class="title">
                                        <h3>Alapcsomag</h3>
                                    </div>
                                    <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                                    <div class="price">
                                        <p><span>Itt dolgozunk neked a legolcsóbban</span></p>
                                    </div>


                                </div>

                                <div class="bottom-content text-center">
                                    <ul class="features list-unstyled">
                                        <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook hirdetéskezelés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + szöveg</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                                    </ul>
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" id="price2" data-aos="fade-up">

                                <div class="top-content text-center">

                                    <div class="title">
                                        <h3>Busniess csomag</h3>
                                    </div>
                                    <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                                    <div class="price">
                                        <span>Itt dolgozunk neked a legtöbbet!</span>
                                    </div>


                                </div>

                                <div class="bottom-content text-center">
                                    <ul class="features list-unstyled">
                                        <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue"></i>
                                        <i class="fa fa-instagram fa-3x mb-3 animated bounce" aria-hidden="true" style="color: saddlebrown"></i>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook + Instagram hirdetéskezelés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Faceboook + Instagram posztok készítése reklámszövegíróval (heti 3 poszt)</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + KREATÍV szövegírás</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                                    </ul>
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-left">

                                <div class="top-content text-center">

                                    <div class="title">
                                        <h3>Prémium csomag</h3>
                                    </div>
                                    <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                                    <div class="price">
                                        <span>Szinte korlátlan hozzánkférés!</span>
                                    </div>


                                </div>

                                <div class="bottom-content text-center">
                                    <ul class="features list-unstyled">
                                        <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                        <i class="fa fa-instagram fa-3x mb-3 animated bounce" aria-hidden="true" style="color: saddlebrown;"></i>
                                        <i class="fa fa-comment fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook + Instagram hirdetéskezelés</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Faceboook + Instagram posztok készítése reklámszövegíróval (napi 1 poszt)</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + KREATÍV szövegírás</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebok & Instagram messenger kezelése</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                        <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                                    </ul>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </section>
        </div>
    </div>
    <hr>
    <div class="row">
       <div class="col-md-12 col-xs-12">
           <section id="prices">
               <div class="container">

                   <div class="row">
                       <div class="col-lg-6 col-lg-offset-3 text-center">
                           <h2 class="section-title"><span class=" ion-minus"></span>Webfejlesztés!<span class="ion-minus"></span></h2>
                           <p>SEO & MOBIL barát.</p><br>
                       </div>
                   </div>


                   <div class="prices-box">
                       <div class="row">

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-right">

                               <div class="top-content text-center">

                                   <div class="title">
                                       <h3>Statikus oldal</h3>
                                   </div>

                                   <div class="price">
                                       <span>Egyedi ajánlat</span>
                                   </div>

                                   <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                               </div>

                               <div class="bottom-content text-center">
                                   <ul class="features list-unstyled">
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>

                                   </ul>
                               </div>

                           </div>

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" id="price2" data-aos="fade-up">

                               <div class="top-content text-center">

                                   <div class="title">
                                       <h3>Blog | CMS</h3>
                                   </div>

                                   <div class="price">
                                       <span>Egyedi ajánlat</span>
                                   </div>

                                   <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                               </div>

                               <div class="bottom-content text-center">
                                   <ul class="features list-unstyled">
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Admin felület</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Extra tárhely méret (igény esetén)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Themeforest design (igény esetén)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Dinamikus adatkezelés</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Biztonságos adatbázis elérés</li>

                                   </ul>
                               </div>

                           </div>

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-left">

                               <div class="top-content text-center">

                                   <div class="title">
                                       <h3>Webshop</h3>
                                   </div>

                                   <div class="price">
                                       <span>Egyedi ajánlat</span>
                                   </div>

                                   <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                               </div>

                               <div class="bottom-content text-center">
                                   <ul class="features list-unstyled">
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Admin felület</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Extra tárhely méret (igény esetén)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Themeforest design (igény esetén)</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Dinamikus adatkezelés</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Biztonságos adatbázis elérés</li>
                                       <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Webáruház funkciók</li>
                                   </ul>
                               </div>

                           </div>

                       </div>
                   </div>

               </div>
           </section>
       </div>
    </div>
@endsection
<script>
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

</script>