@extends('main')

@section('title', '| Collettivo')
@section('description','Online marketing és webfejlesztés. SEO & Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')
    <div class="container">
        <div class="row">
        <div id="collettivo" class="carousel slide animated bounceInLeft" data-ride="carousel">

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="/images/facebook_cover_photo_2.png" alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
                </div>

            </div>


        </div>
        </div>
    </div>

<section>
        <div class="row">
            <div class="col-md-8">

                <p class="lead" style="margin-top: 15px;">Ügynökségünk teljeskörű szolgáltatást nyújt a céged fejlesztésével kapcsolatban. Mi azt a filozófiát képviseljük, hogy mindenki csinálja azt, amihez a legjobban ért – te foglalkozz a vállalkozásoddal, mi bástyaként mögötted segítünk, hogy megtudja a világ: abban amit csinálsz, te vagy a legjobb! Számíthatsz ránk a céged IT rendszerének fejlesztésében az IT audittól a rendszergazdai szolgáltatásig. Marketing tevékenységünk kiterjed a stratégiatervezéstől és tanácsadástól egészen az online-offline marketing kampányok kivitelezésén át a szövegírási feladatokig.
                    Ha weboldalra-webshopra van szükséged, akkor is a legjobb helyen jársz, hiszen marketinges sales-es szemléletünk átjárja az üzletágaink minden porcikáját, hiszen a te célod a profit, a miénk pedig, hogy sikerre vigyük a vállalkozásodat!</p>

            </div>

            <div class="col-md-3 col-md-offset-1 " style="margin-top: 15px;">

                <div class="progress animated bounceInRight ">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Rövid határidő 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Rugalmasság 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Kiemelkedő marketing 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Modern technológiák 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Ár-érték arány 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Laravel | PHP 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">HTML5 | CSS & SASS 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">BOOTSTRAP 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">MySQL 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Wordpress 100%</div>
                </div>
                <div class="progress animated bounceInRight">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 100%; background: #57a544;font-size: 15px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">JavaScript 100%</div>
                </div>
            </div>

        </div>
</section>
    <!-- Image Section - set the background image for the header in the line below -->
    <section class="py-5 bg-image-full img-responsive how-img" style="background-image: url('/images/index2.jpg');">
        <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
        <div style="height: 200px;"></div>
    </section>

    <!-- Timeline -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase section-title h1">Miért a COLLETTIVO?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive img-circle img-thumbnail" src="/images/pinterest_profile_image.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>NEKED SZÓL?</h4>
                                    <h4 class="subheading">NEKED!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Digitális ügynökségünk dinamikus, fejlődni akaró cégek számára hoz okos és bevételnövelő megoldásokat a webről.</p>

                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive img-circle img-thumbnail" src="../images/pinterest_profile_image.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>SEO & MOBIL</h4>
                                    <h4 class="subheading">RESZPONZÍV WEBOLDALAK FEJLESZTÉSE</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Számunkra az egyik legfontosabb, hogy ne csak weboldalt, webáruházat, vagy landing oldalakalat készítsünk, hanem egy hatékony marketingeszközt adjunk át megrendelőinknek.</p>

                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive img-circle img-thumbnail" src="../images/pinterest_profile_image.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Online Marketing</h4>
                                    <h4 class="subheading">Hirdess csak azoknak, akik majd a vevőid lehetnek!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Mivel a magyar felnőtt lakosság 90%-a Social média felhasználó, így rengeteg embert elérhetünk a posztjainkkal, kampányainkkal. Éppen ezért számunkra olyan a Facebook, Instagram, Youtube, Google Ads, mint a mesterlövészet. Valóban azokhoz az emberekhez szólunk, akik potenciális vásárlókká tudnak válni, tehát precízen kell célozni a csoportunkra.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive img-circle img-thumbnail" src="../images/pinterest_profile_image.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Mi AZ A SEO?</h4>
                                    <h4 class="subheading">A KERESŐMARKETING A 21. SZÁZAD MARKETINGJE</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">A SEO (Search Engine Optimization), vagyis keresőoptimalizálás célja, hogy weboldala a releváns kifejezésekre minél jobb helyen szerepeljen a keresőmotorok találati listáján. Számos keresőmotor létezik, azonban a Google motorja messze a leghasználtabb a világon, de a tengerentúlon a Bing és a Yahoo motorjai is népszerűek. Minél előbb szerepel oldala a találati listán, annál több felhasználó fog rákattintani vállalkozására. Magyarországon az internetezők 97%-a a Google keresőjét hasznája, így irodánk leginkább erre a motorra koncentrál.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Egyedi
                                    <br>webes
                                    <br>megoldások!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>



@stop

