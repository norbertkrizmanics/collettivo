@extends('main')

@section('title', '| Elérhetőség')
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')


@section('content')
    <div class="row text-center">
    <h1>
        <a href="" class="typewrite section-title h1" data-period="2000" data-type='[ "ELÉRHETŐSÉG" ]'>
            <span class="wrap"></span>

        </a>
    </h1>
        <p class="text-center w-75 m-auto">Vegye fel a kapcsolatot a Collettivo csapatával.</p>

    </div>
    <hr>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- Image Section - set the background image for the header in the line below -->
            <section class="py-5 bg-image-full img-responsive how-img" style="background-image: url('/images/contact.jpg');" >
                <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
                <div style="height: 200px;">

                </div>

            </section>
        </div>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                    <div class="card border-0">
                        <div class="card-body text-center">
                            <i class="fa fa-phone fa-5x mb-3 animated bounce" aria-hidden="true"></i>
                            <h4 class="text-uppercase mb-5">Hívjon minket</h4>
                            <p>+36-30/587-7008</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                    <div class="card border-0">
                        <div class="card-body text-center">
                            <i class="fa fa-facebook-square fa-5x mb-3 animated bounce" aria-hidden="true"></i>
                            <h4 class="text-uppercase mb-5">Social</h4>
                            <address><a href="https://www.facebook.com/collettivohungary/" target="_blank">Collettivo</a> </address>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                    <div class="card border-0">
                        <div class="card-body text-center">
                            <i class="fa fa-map-marker fa-5x mb-3 animated bounce" aria-hidden="true"></i>
                            <h4 class="text-uppercase mb-5">Lokáció</h4>
                            <address>Budapest </address>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 my-5">
                    <div class="card border-0">
                        <div class="card-body text-center">
                            <i class="fa fa-at fa-5x mb-3 animated bounce" aria-hidden="true"></i>
                            <h4 class="text-uppercase mb-5">Írjon nekünk</h4>
                            <p>info@collettivo.hu</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Team -->
    <section id="team" class="pb-5">
        <div class="container">
            <h5 class="section-title h1">Az arcunkat adjuk minden projekthez.</h5>
            <div class="row">
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid img-thumbnail img-responsive mx-auto" src="../images/Barbara.jpg" alt="card image"></p>
                                        <h4 class="card-title">Rádai Barbara</h4>
                                        <p class="card-text">Online Marketing</p>
                                        <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Szia! Barbi vagyok.</h4>
                                        <p class="card-text">Az oldaladat Norbi elékészíti neked olyanra amilyenre szeretnéd. Én viszont arrol fogok gondoskodni, hogy minnél több emberhez juthasson el az oldalad.A tartalma pedig a legprofibb legyen. Akár angol vagy olasz nyelven is!</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class="img-fluid img-thumbnail img-responsive mx-auto" src="../images/Norbert.png" alt=""></p>
                                        <h4 class="card-title">Krizmanics Norbert</h4>
                                        <p class="card-text">Back-end | Front-end </p>
                                        <a href="#" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Szia! Norbi vagyok.</h4>
                                        <p class="card-text">Az én feladatom, hogy teljesítsem a webfejlesztés kérésedet,de nyugi elég csak azt tudnod,hogy mit szeretnél. A többit bízd rám.Okos lesz a back-ended!Modern mobil barát pedig a front-ended!A tartalmat Barbi tökéletesíti az oldaladra.</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->

            </div>
        </div>
    </section>
    <!-- Team -->

        <div class="row">
            <div class="col-md-4 text-center">
                <h3>Visszahívunk!</h3>
                <p>
                    Számunkra fontos, hogy ügyfeleink megtalálják a tökéletes megoldást. Kérünk, a következő lépésben töltsd ki az ajánlatkérő űrlapot, hamarosan felvesszük veled a kapcsolatot, hogy személyre szabott szolgáltatást tudjunk ajánlani.
                </p>

            </div>
            <div class="col-md-8">
                <h3>Írjon nekünk</h3>
                <hr>
                <form action="{{ url('contact') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label name="name">Név:</label>
                        <input id="name" name="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="email">Email:</label>
                        <input id="email" name="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="subject">Tárgy:</label>
                        <input id="subject" name="subject" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="message">Üzenet:</label>
                        <textarea id="message" name="message" class="form-control" placeholder="Irja be az üzenetét..."></textarea>
                    </div>

                    <input type="submit" value="Küldés" class="btn btn-success">
                </form>
            </div>
        </div>


    </section>
@endsection
<script>
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script>