@extends('main')
@section('title','| GDPR')
@section('content')
    <div class="row text-center">
        <div class="col-md-12">
            <h1 class="section-title h1">GDPR</h1>
            <h2> ADATKEZELÉSI TÁJÉKOZTATÓ</h2>
            <p class="lead">


                A jelen tájékoztató célja, hogy rögzítse a collettivo.hu/gdpr (a továbbiakban weboldal) üzemeltetője, Odú-hungary Kft. (a továbbiakban Szolgáltató) által alkalmazott adatvédelmi és -kezelési elveket és a Szolgáltató adatvédelmi és -kezelési politikáját, amelyet a Szolgáltató magára nézve kötelezőnek ismer el, mivel a Szolgáltató célja, hogy a lehető legteljesebb mértékben biztosítsa a weboldalon regisztrált felhasználók személyes adatainak védelmét.
                A weboldal használatával, valamely szolgáltatásának, alkalmazásának igénybevételével a Felhasználó hozzájárul, hogy személyes adatai kezelésére a jelen Adatkezelési Tájékoztató rendelkezései szerint kerüljön sor.
</p>
            <p class="lead">
                <h3>I. A SZOLGÁLTATÓ MINT ADATKEZELŐ ADATAI</h3>
                A Szolgáltató cégneve: Odú-hungary Kft.
                A Szolgáltató székhelye: 1022 Budapest Lévay utca 5/a
                A Szolgáltató adószáma: 25539682241
                A Szolgáltató telefonos elérhetősége: +36305877008
                E-mail: info@collettivo.hu
            </p>
            <p class="lead">
               <h3> II. FOGALOM MEGHATÁROZÁSOK</h3>
                A jelen tájékoztatóban használt kifejezéseket az információs önrendelkezési jogról és az információszabadságról szóló 2011. évi CXII. törvény (a továbbiakban Info törvény) értelmező rendelkezései között meghatározott fogalmak szerint kell értelmezni az alábbiak szerint.
                Személyes adat: az érintettel kapcsolatba hozható adat – különösen az érintett neve, azonosító jele, valamint egy vagy több fizikai, fiziológiai, mentális, gazdasági, kulturális vagy szociális azonosságára jellemző ismeret -, valamint az adatból levonható, az érintettre vonatkozó következtetés;
                Érintett: bármely meghatározott, személyes adat alapján azonosított vagy – közvetlenül vagy közvetve – azonosítható természetes személy (jelen tájékoztató esetében a Felhasználó);
                Hozzájárulás: az érintett akaratának önkéntes és határozott kinyilvánítása, amely megfelelő tájékoztatáson alapul, és amellyel félreérthetetlen beleegyezését adja a rá vonatkozó személyes adatok – teljes körű vagy egyes műveletekre kiterjedő – kezeléséhez;
                Adatkezelés: az alkalmazott eljárástól függetlenül az adatokon végzett bármely művelet vagy a műveletek összessége, így különösen gyűjtése, felvétele, rögzítése, rendszerezése, tárolása, megváltoztatása, felhasználása, lekérdezése, továbbítása, nyilvánosságra hozatala, összehangolása vagy összekapcsolása, zárolása, törlése és megsemmisítése, valamint az adatok további felhasználásának megakadályozása, fénykép-, hang- vagy képfelvétel készítése, valamint a személy azonosítására alkalmas fizikai jellemzők (pl. ujj- vagy tenyérnyomat, DNS-minta, íriszkép) rögzítése;
                Adatkezelő: az a természetes vagy jogi személy, illetve jogi személyiséggel nem rendelkező szervezet, aki vagy amely önállóan vagy másokkal együtt az adatok kezelésének célját meghatározza, az adatkezelésre (beleértve a felhasznált eszközt) vonatkozó döntéseket meghozza és végrehajtja, vagy az általa megbízott adatfeldolgozóval végrehajtatja (jelen tájékoztató esetében a Szolgáltató);
                Adattovábbítás: az adat meghatározott harmadik személy számára történő hozzáférhetővé tétele;
                Nyilvánosságra hozatal: az adat bárki számára történő hozzáférhetővé tétele;
                Adattörlés: az adatok felismerhetetlenné tétele oly módon, hogy a helyreállításuk többé nem lehetséges;
                Adatmegjelölés: az adat azonosító jelzéssel ellátása annak megkülönböztetése céljából;
                Adatzárolás: az adat azonosító jelzéssel ellátása további kezelésének végleges vagy meghatározott időre történő korlátozása céljából;
                Adatfeldolgozás: az adatkezelési műveletekhez kapcsolódó technikai feladatok elvégzése, függetlenül a műveletek végrehajtásához alkalmazott módszertől és eszköztől, valamint az alkalmazás helyétől, feltéve hogy a technikai feladatot az adatokon végzik;
                Adatfeldolgozó: az a természetes vagy jogi személy, illetve jogi személyiséggel nem rendelkező szervezet, aki vagy amely az adatkezelővel kötött szerződése alapján – beleértve a jogszabály rendelkezése alapján történő szerződéskötést is – adatok feldolgozását végzi;
            </p>
            <p class="lead">
               <h3> III. ADATKEZELÉS ELVE</h3>
                A Szolgáltató a rögzített személyes adatokat a mindenkor hatályos adatvédelmi jogszabályokkal – így különösen az Info törvénnyel – összhangban, a jelen tájékoztatónak megfelelően kezeli.
            </p>
            <p class="lead">
                <h3>IV. ADATKEZELÉS JOGALAPJA</h3>
                A weboldal működésével és szolgáltatásaival kapcsolatos adatkezelés során a személyes adatok felvétele és kezelése az érintett önkéntes hozzájárulásán alapul.
                A Felhasználó a hozzájárulást a weboldal szolgáltatásának igénybevételével (regisztrációval) adja meg.
                A Szolgáltató jogosult a Felhasználó részére hírlevelet vagy egyéb reklám célú levelet küldeni, amennyiben a Felhasználó a regisztrációkor a megfelelő adatai megadásával ehhez előzetesen, egyértelműen, kifejezetten és önkéntesen hozzájárult. A Szolgáltató nem köteles ellenőrizni, hogy a Felhasználó által a regisztrációkor, illetőleg egyébként a hozzájárulás megadásakor megadott adatok valósak, illetőleg helytállóak.
                A Felhasználó az előző pontban hivatkozott önkéntes hozzájárulását bármikor jogosult visszavonni. Ebben az esetben a Szolgáltató a visszavonást követően a Felhasználó részére több hírlevelet, illetőleg egyéb reklámlevelet nem küld, továbbá törli a Felhasználó adatait a hírlevélre feliratkozott felhasználók adatai közül.
                A felvett személyes adatokat törvény eltérő rendelkezésének hiányában a Szolgáltató a rá vonatkozó jogi kötelezettség (így különösen a számviteli kötelezettség, Felhasználóval létrejött szerződéses kötelezettség) teljesítése céljából, illetve saját vagy harmadik személy jogos érdekének érvényesítése céljából, ha ezen érdek érvényesítése a személyes adatok védelméhez fűződő jog korlátozásával arányban áll, további külön hozzájárulás nélkül, valamint a Felhasználó hozzájárulásának visszavonását követően is kezelheti.
            </p>
            <p class="lead">
               <h3> V. A KEZELT ADATOK KÖRE</h3>
                A Szolgáltató az általa nyújtott szolgáltatás teljesítése során és azt követően a Felhasználó önkéntes hozzájárulása alapján a Felhasználó alábbi személyes adatait kezeli: vezetéknév, keresztnév, e-mail cím.
                A személyes adatok valódiságáért, pontosságáért kizárólag a Felhasználó tartozik felelősséggel.
            </p>
            <p class="lead">
               <h3> VI. AZ ADATKEZELÉS CÉLJA</h3>
                A Felhasználó személyes adatai kezelésének célja a IV. pontban foglalt hírlevél és reklám célú levél küldése és a Szolgáltató egyéb jogi kötelezettségének teljesítése. A Szolgáltató illetéktelen harmadik személyeknek a Felhasználó személyes adatait nem szolgáltatja ki.
            </p>
            <p class="lead">


               <h3> VII. ADATKEZELÉS IDŐTARTAMA, ADATOK MÓDOSÍTÁSA, TÖRLÉSE, TILTAKOZÁS AZ ADATKEZELÉS ELLEN</h3>
                A Szolgáltató a szolgáltatások igénybevételével kapcsolatban kezelt személyes adatokat az adatkezelés céljának megvalósulásáig kezeli.
                A regisztráció során megadott személyes adatait a Felhasználó a weboldalon történő belépését követően bármikor módosíthatja.
                A Felhasználó az info@collettivo.hu email címre küldött elektronikus levél útján kérheti személyes adatai törlését a Szolgáltatótól. A Felhasználó személyes adatait az érintett kérelme nélkül is törli a Szolgáltató, ha annak kezelése jogellenes, az adatkezelés célja megszűnt, vagy az adatok tárolásának törvényben meghatározott határideje lejárt, azt a bíróság vagy a Nemzeti Adatvédelmi és Információszabadság Hatóság elrendelte, vagy ha az adatkezelés hiányos vagy téves – és ez az állapot jogszerűen nem orvosolható –, feltéve, hogy a törlést törvény nem zárja ki.
                Törlés helyett a Szolgáltató zárolja a személyes adatot, ha a Felhasználó ezt kéri, vagy ha a rendelkezésére álló információk alapján feltételezhető, hogy a törlés sértené a Felhasználó jogos érdekeit. Az így zárolt személyes adatot kizárólag addig kezeli a Szolgáltató, ameddig fennáll az az adatkezelési cél, amely a személyes adat törlését kizárja. A Felhasználó hozzájárulásának visszavonását követően a Szolgáltató a jogszabályban foglalt kötelezettségeinek (így különösen a számviteli kötelezettségek) teljesítése érdekében a továbbiakban is kezelheti az érintettre vonatkozó személyes adatokat.
                A Felhasználó tiltakozhat személyes adatainak kezelése ellen az info@collettivo.hu email címre küldött elektronikus levél útján,
                – ha a személyes adatok kezelése vagy továbbítása kizárólag a Szolgáltatóra vonatkozó jogi kötelezettség teljesítéséhez vagy az adatkezelő, adatátvevő vagy harmadik személy jogos érdekének érvényesítéséhez szükséges, kivéve az ún. kötelező adatkezelés esetét;
                – ha a személyes adat felhasználása vagy továbbítása közvetlen üzletszerzés, közvélemény-kutatás vagy tudományos kutatás céljára történik; valamint
                – valamely törvényben meghatározott egyéb esetben.
                Az adatkezelő a tiltakozást a kérelem benyújtásától számított legrövidebb időn belül, de legfeljebb 15 napon belül megvizsgálja, annak megalapozottsága kérdésében döntést hoz, és döntéséről a kérelmezőt írásban tájékoztatja.
                Ha az adatkezelő az érintett tiltakozásának megalapozottságát megállapítja, az adatkezelést – beleértve a további adatfelvételt és adattovábbítást is – megszünteti, és az adatokat zárolja, valamint a tiltakozásról, továbbá az annak alapján tett intézkedésekről értesíti mindazokat, akik részére a tiltakozással érintett személyes adatot korábban továbbította, és akik kötelesek intézkedni a tiltakozási jog érvényesítése érdekében.
                Ha az érintett az adatkezelő meghozott döntésével nem ért egyet, illetve ha az adatkezelő a 15 napos határidőt elmulasztja, az érintett – a döntés közlésétől, illetve a határidő utolsó napjától számított 30 napon belül bírósághoz fordulhat. A bíróság soron kívül jár el.
                Ha az adatátvevő jogának érvényesítéséhez szükséges adatokat az érintett tiltakozása miatt nem kapja meg, az értesítés közlésétől számított 15 napon belül, az adatokhoz való hozzájutás érdekében bírósághoz fordulhat az adatkezelő ellen. Az adatkezelő az érintettet is perbe hívhatja.
                Ha az adatkezelő az értesítést elmulasztja, az adatátvevő felvilágosítást kérhet az adatátadás meghiúsulásával kapcsolatos körülményekről az adatkezelőtől, amely felvilágosítást az adatkezelő az adatátvevő erre irányuló kérelmének kézbesítését követő 8 napon belül köteles megadni. Felvilágosítás kérése esetén az adatátvevő a felvilágosítás megadásától, de legkésőbb az arra nyitva álló határidőtől számított 15 napon belül fordulhat bírósághoz az adatkezelő ellen. Az adatkezelő az érintettet is perbe hívhatja.
                Az adatkezelő az érintett adatát nem törölheti, ha az adatkezelést törvény rendelte el. Az adat azonban nem továbbítható az adatátvevő részére, ha az adatkezelő egyetértett a tiltakozással, vagy a bíróság a tiltakozás jogosságát megállapította.
            </p>
            <p class="lead">
               <h3> VIII. TÁJÉKOZTATÁS KÉRÉSE</h3>
                A Felhasználó bármikor jogosult tájékoztatást kérni a Szolgáltató által a weboldal szolgáltatásaival összefüggésben kezelt, rá vonatkozó személyes adatokról az info@collettivo.hu email címen vagy a +36 30 5877008 telefonszámon. A Szolgáltató a Felhasználó kérelmére tájékoztatást ad a Felhasználóra vonatkozó, az adott szolgáltatással összefüggésben általa kezelt adatokról, azok forrásáról, az adatkezelés céljáról, jogalapjáról, időtartamáról, az adatfeldolgozó nevéről, címéről, az adattovábbítás jogalapjáról és címzettjéről, az adatkezeléssel összefüggő tevékenységéről. A Szolgáltató köteles a kérelem benyújtásától számított legrövidebb idő alatt, legfeljebb azonban 30 napon belül, közérthető formában, az érintett erre irányuló kérelmére írásban megadni a tájékoztatást. A tájékoztatás ingyenes. A korábban a Szolgáltató részére megadott e-mail címről érkezett igényt a Szolgáltató a Felhasználótól érkezett igénynek tekinti. Egyéb email címről, valamint írásban benyújtott igények esetén a Felhasználó akkor nyújthat be igényt, ha megfelelően igazolta felhasználói minőségét.

                JOGÉRVÉNYESÍTÉSI LEHETŐSÉGEK
                A Felhasználó jogérvényesíti lehetőségeit az Info törvény, valamint az 1959. évi IV. tv. (Ptk.) alapján bíróság előtt gyakorolhatja, továbbá bármilyen személyes adattal kapcsolatos kérdésben kérheti a Nemzeti Adatvédelmi és Információszabadság Hatóság segítségét is (1125 Budapest Szilágyi Erzsébet fasor 22/C, postacím: 1530 Budapest, Pf. 5., ugyfelszolgalat@naih.hu, (+36) 1 391 1400, www.naih.hu).
            </p>
        </div>
    </div>
@endsection