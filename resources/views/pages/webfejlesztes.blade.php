@extends('main')

@section('title', '| Webfejlesztés')
@section('description','Online marketing és webfejlesztés. Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')
@section('content')

        <div class="row">
            <div class="col-md-12 text-center">
                <h1 CLASS="section-title h1">SEO & MOBIL barát webfejlesztés</h1>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12 col-xs-12">
            <!-- Image Section - set the background image for the header in the line below -->
            <section class="py-5 bg-image-full img-responsive how-img" style="background-image: url('/images/webdev2.jpg');" >
                <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
                <div style="height: 200px;">

                </div>

            </section>
                </div>
            </div>
            <section>
            <div class="col-md-6">
            <p>Az, hogy egy vállalkozó, egy cég, egy művész vagy egy sportoló fenn legyen a weben a 21.században alapkövetelmény. Mindig arra törekszünk, hogy ne csak fenn legyél a weben, hanem legyen eredménye is és elérd a céljaidat. Tele van az internet ingyenes weboldal készítő programokkal, sablonokkal, de a tapasztalat azt mutatja, hogy nem működnek. Ha Te is jobban szereted a megbízhatóságot, kérj tőlünk egy konzultációs időpontot.</p>
            </div>
            <div class="col-md-6">
            <p>Weboldal készítés megrendelése előtt érdemes egy kicsit tájékozódni arról, hogy mi az, amit sikeres cégek használnak, mik azok a funkciók amik ma már szinte kötelezőek és milyen szempontoknak kell megfelelnie egy jó weboldalnak. Ha már ez lesz az 5. weboldalad, akkor valószínűleg erre nincs szükség, de ha még csak most kóstolsz bele az online világba, akkor mindenképpen javasoljuk, hogy kérj egy ingyenes konzultációt ahol kötöttségek nélkül átbeszéljük, mi pl. a reszponzivitás, a Google Analytics, milyen struktúrát és stílust érdemes választanod, hogyan válassz ideális kulcsszavakat, hogyan írj profi tartalmat az oldaladra és még sok mást…</p>
            </div>
            </section>
            </div>

        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-code"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">PHP | Laravel | JavaScript</h4>
                        <p>A PHP egy általános szerveroldali szkriptnyelv dinamikus weblapok készítésére. Az első szkriptnyelvek egyike, amely külső fájl használata helyett HTML oldalba ágyazható. A kódot a webszerver PHP feldolgozómodulja értelmezi, ezzel dinamikus weboldalakat hozva létre. </p>
                        <p>PHP-alapú LARAVEL programozás, egyedi fejlesztésű rendszerek.</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-html5"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">Html5 | css3</h4>
                        <p>A CSS ( Cascading Style Sheets, magyarul: egymásba ágyazott stíluslapok) a számítástechnikában egy stílusleíró nyelv, mely a HTML5 típusú strukturált dokumentumok megjelenését írja le. Profi design = CSS3!</p>
                        <br><br><br><br>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-bold"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">Bootstrap</h4>
                        <p>Bootstrap egy HTML, CSS, JS keretrendszer responzív "mobile first" projekt a weben. A Bootstrap keretrendszertől, lesz csak igazán MOBIL barát a weboldalad.</p>
                        <br><br><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-shopping-cart"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">Webshop</h4>
                        <p>Az OpenCart egy nyílt forráskódú e-kereskedelmi rendszer. Segítségével gyorsan és olcsón lehet webáruházat létrehozni, hiszen már az alapcsomag is számos funkcióval rendelkezik, de moduláris felépítése miatt egyedi funkciókkal és kész pluginekkel is tovább bővíthető.</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-file"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">Statikus oldal</h4>
                        <p>Magán, céges bemutató weboldalak alapfunkciókkal admin felület nélkül. Rövid határidő. Modern design. SEO és MOBIL barát.</p>
                        <br><br><br>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="box">
                    <div class="box-icon animated bounce">
                        <span class="fa fa-4x fa-wordpress"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">Blog | CMS</h4>
                        <p>Honlapjaink mögött saját fejlesztésű tartalomkezelő keretrendszer, a COLLETTIVO CMS áll. A jól átlátható adminisztrációs felület használata csupán alapvető számítástechnikai tudást igényel. A rendszer kezelését megtanítjuk, majd a későbbiekben is támogatást nyújtunk a CMS-ben történő eligazodáshoz. </p>
                    </div>
                </div>
            </div>
        </div>

        <section id="prices">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 text-center">
                        <h2 class="section-title"><span class=" ion-minus"></span>Korrekt árak!<span class="ion-minus"></span></h2>
                        <p>Kérjen ingyenes visszahívást!  </p><br>
                    </div>
                </div>


                <div class="prices-box">
                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-right">

                            <div class="top-content text-center">

                                <div class="title">
                                    <h3>Statikus oldal</h3>
                                </div>

                                <div class="price">
                                    <span>Egyedi ajánlat</span>
                                </div>

                                <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            </div>

                            <div class="bottom-content text-center">
                                <ul class="features list-unstyled">
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>

                                </ul>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" id="price2" data-aos="fade-up">

                            <div class="top-content text-center">

                                <div class="title">
                                    <h3>Blog | CMS</h3>
                                </div>

                                <div class="price">
                                    <span>Egyedi ajánlat</span>
                                </div>

                                <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            </div>

                            <div class="bottom-content text-center">
                                <ul class="features list-unstyled">
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Admin felület</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Extra tárhely méret (igény esetén)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Themeforest design (igény esetén)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Dinamikus adatkezelés</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Biztonságos adatbázis elérés</li>

                                </ul>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-left">

                            <div class="top-content text-center">

                                <div class="title">
                                    <h3>Webshop</h3>
                                </div>

                                <div class="price">
                                    <span>Egyedi ajánlat</span>
                                </div>

                                <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            </div>

                            <div class="bottom-content text-center">
                                <ul class="features list-unstyled">
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Keresőoptimalizálás</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Domain név és tárhely</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Reszponzív kialakítású weblap</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Garancia</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Angol, Olasz, Magyar nyelvű</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Admin felület</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Oldaltérkép</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Közösségi média megosztó gombok beépítése</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Prémium dizájn és funkciók</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Választható vagy egyedi design (részletes testreszabással)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Igény szerint Online Marketing tevékenység</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Extra tárhely méret (igény esetén)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Themeforest design (igény esetén)</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Dinamikus adatkezelés</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>GDPR</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SSL</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Biztonságos adatbázis elérés</li>
                                    <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Webáruház funkciók</li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

@endsection
