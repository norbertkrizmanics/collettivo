@extends('main')

@section('title', '| Online Marketing | SEO')
@section('description','Online marketing és webfejlesztés. SEO & Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')

    <div class="how-section1">
        <div class="row">
            <h1 class="section-title h1">ONLINE MARKETING & SEO</h1>
            <hr>
            <div class="col-md-6 how-img">

                <img class="rounded mx-auto d-block img-thumbnail" src="/images/seo.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
            </div>
            <div class="col-md-6">
                <h4>SEO</h4>
                <h4 class="subheading">KERESŐOPTIMALIZÁLÁS</h4>
                <p class="text-muted">Kiszámoltad már, hogy mennyit ér Neked a TOP 10-ben lenni? És a TOP 3-ban? Biztosak vagyunk benne, hogy többet, mint amennyibe sikerdíjas szolgáltatásunk kerül. Vágj bele most.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4>FACEBOOK | INSTAGRAM | YOUTUBE</h4>
                <h4 class="subheading">Social babysitting (postolás & ellenőrzés).</h4>
                <p class="text-muted">
                    A közösségi média olyan mint egy kis bébi. Sok fejfájást okoz. Odafigyelést igényel minden percben és dajkálgatást naponta. Facebook, Twitter, Instagram, Youtube, Trip Advisor, Airbnb ésatöbbi. Mi éberen figyelünk, hogy minden rendben legyen, tartalmat generálunk és postolunk Neked.</p>
            </div>
            <div class="col-md-6 how-img">
                <img class="rounded mx-auto d-block img-thumbnail" src="/images/social.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 how-img">
                <img class="rounded mx-auto d-block img-thumbnail" src="/images/blog.jpg"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
            </div>
            <div class="col-md-6">
                <h4>BLOG ÍRÁS</h4>
                <h4 class="subheading">Hatékonyabb kommunikáció</h4>
                <p class="text-muted">Ismerd meg a céges blogírásban rejlő lehetőségeket, amelyekkel vállalkozásodnak nemcsak a bevételét növelheted meg, de egyúttal sokkal nagyobb lesz annak szakmai tekintélye is!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4> REKLÁMVIDEÓK | ÜZLETI ÉS TERMÉK FOTÓZÁS</h4>
                <h4 class="subheading">DJI & NIKON</h4>
                <p class="text-muted">Imázs, <strong>drón</strong>, promóció, rendezvény, animáció, gerilla marketing. Mindennek alapeszköze a videó. Az emberek már nem szeretnek sokat olvasni, vetíts nekik valami érdekeset inkább és imádni fognak!Fotók nélkül nem az igazi a webes és a nyomtatott megjelenés sem. Tegyél a szolgáltatások és termékek mögé arcokat és szerethetővé válsz. Régi trükk, de mindig működik!</p>
            </div>
            <div class="col-md-6 how-img">
                <img class="rounded mx-auto d-block img-thumbnail" src="/images/photo.jpg"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 how-img">
                <img class="rounded mx-auto d-block img-thumbnail" src="/images/piackutatas.png"  alt="Collettio" title="Collettivo - Online marketing | SEO & MOBIL barát webfejlesztés">
            </div>
            <div class="col-md-6">
                <h4>PIACKUTATÁS | VERSENYTÁRSELEMZÉS</h4>
                <h4 class="subheading">Google Analytics statisztikák</h4>
                <p class="text-muted">A számok beszélnek. Ha nem méred a konverziót és a teljesítményt és ha fogalmad sincsen arról mi az a CTR vagy CPC, akkor minden sokkal drágább és nehezebb lesz. Az Analytics napról napra változik, ha nem ez a munkád esélyed sincs követni mi hogyan működik. Csak kapaszkodj belénk, mi megmutatjuk az utat</p>
            </div>
        </div>
    </div>
    <div class="container text-center">
        <div class="row">
            <h1 class="section-title h1">Nem csak a levegőbe beszélünk.</h1>
            <p>Büszkék vagyunk azokra akikkel együtt dolgozhattunk.</p>
        </div>
    </div>
<div class="container">
    <div class="row">
        <div class="column">
            <div class="card-marketing">
                <p><i class="fa fa-user fa-5x mb-3"></i></p>
                <h3>11+</h3>
                <p>Partner</p>
            </div>
        </div>

        <div class="column">
            <div class="card-marketing">
                <p><i class="fa fa-check fa-5x mb-3"></i></p>
                <h3>20+</h3>
                <p>Projekt</p>
            </div>
        </div>

        <div class="column">
            <div class="card-marketing">
                <p><i class="fa fa-smile-o fa-5x mb-3"></i></p>
                <h3>20+</h3>
                <p>Elégedett ügyfél</p>
            </div>
        </div>

        <div class="column">
            <div class="card-marketing">
                <p><i class="fa fa-coffee fa-5x mb-3"></i></p>
                <h3>55+</h3>
                <p>Meeting</p>
            </div>
        </div>
    </div>
</div>
    <section id="prices">
        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 text-center">
                    <h2 class="section-title"><span class=" ion-minus"></span>Egyedi kedvező SOCIAL árak!<span class="ion-minus"></span></h2>
                    <p>Minden projekt más. Ezért az egyedi árakban hiszünk. </p><br>
                </div>
            </div>


            <div class="prices-box">
                <div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-right">

                        <div class="top-content text-center">

                            <div class="title">
                                <h3>Alapcsomag</h3>
                            </div>
                            <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            <div class="price">
                                <p><span>Itt dolgozunk neked a legolcsóbban</span></p>
                            </div>


                        </div>

                        <div class="bottom-content text-center">
                            <ul class="features list-unstyled">
                                <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook hirdetéskezelés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + szöveg</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" id="price2" data-aos="fade-up">

                        <div class="top-content text-center">

                            <div class="title">
                                <h3>Busniess csomag</h3>
                            </div>
                            <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            <div class="price">
                                <span>Itt dolgozunk neked a legtöbbet!</span>
                            </div>


                        </div>

                        <div class="bottom-content text-center">
                            <ul class="features list-unstyled">
                                <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue"></i>
                                <i class="fa fa-instagram fa-3x mb-3 animated bounce" aria-hidden="true" style="color: saddlebrown"></i>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook + Instagram hirdetéskezelés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Faceboook + Instagram posztok készítése reklámszövegíróval (heti 3 poszt)</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + KREATÍV szövegírás</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pd0 price-table aos-init aos-animate" data-aos="fade-left">

                        <div class="top-content text-center">

                            <div class="title">
                                <h3>Prémium csomag</h3>
                            </div>
                            <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>

                            <div class="price">
                                <span>Szinte korlátlan hozzánkférés!</span>
                            </div>


                        </div>

                        <div class="bottom-content text-center">
                            <ul class="features list-unstyled">
                                <i class="fa fa-facebook-square fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                <i class="fa fa-instagram fa-3x mb-3 animated bounce" aria-hidden="true" style="color: saddlebrown;"></i>
                                <i class="fa fa-comment fa-3x mb-3 animated bounce" aria-hidden="true" style="color: darkblue;"></i>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>SEO optimalizálás</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Versenytárselemzés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebook + Instagram hirdetéskezelés</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Faceboook + Instagram posztok készítése reklámszövegíróval (napi 1 poszt)</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Képes hirdetések + KREATÍV szövegírás</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Facebok & Instagram messenger kezelése</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Optimális hirdetési költség javaslat</li>
                                <li><span class="fa fa-4x fa-check" style="color:#87d37c;"></span>Havi jelentések küldése</li>
                            </ul>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <h2 class="section-title h1"><span class=" ion-minus"></span>Egyedi bemutató DRÓNFILM!<span class="ion-minus"></span></h2>

        </div>
    </div>
    <!-- Image Section - set the background image for the header in the line below -->
    <section class="py-5 bg-image-full" style="background-image: url('/images/drone.jpg');">
        <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
        <div style="height: 200px;"></div>
    </section>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <p>Légi fotó- és (drón) filmfelvétel. </p><br>
            <p>Professzionális, látványos légi felvételkészítés  új, technológiai eszközzökkel.</p><br>
        </div>
    </div>

    <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://www.youtube.com/embed/7agNCMzE6Ho" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

    <div class="row text-center">
        <h1 class="section-title h1">Kérjen egyedi árajánlatot!</h1>
        <a class="btn btn-price animated pulse" href="/contact">Árajánlat</a>
    </div>

    </section>
@endsection
