@extends('main')
@section('title','| Portfolio')
@section('description','Online marketing és webfejlesztés. SEO & Mobil barát weboldalak. Legmodernebb keretrendszerek, naprakész szakemberek.Legjobb választás elképzelései megvalósítására a Collettivo csapata.')

@section('content')
<div class="portfolio">
    <div class="container text-center">
        <div class="col-md-12">
            <h1>
                <a href="" class="typewrite section-title h1" data-period="2000" data-type='[ "PORTFOLIONK","MUNKÁINK","REFERENCIÁINK" ]'>
                    <span class="wrap"></span>
                </a>
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 text-center">
                <p>Néhány kedvenc a munkáink közül. </p><br>
            </div>
        </div>
        <hr>
        <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>VIOLA GARDEN VILLA</h5>
                <p>WEBFEJLESZTÉS</p>

                <a href="#">
                    <img class="img-rounded" src="/images/violagardenvilla.png" alt="">
                </a>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>TISZTAKONYHA</h5>
                <p>WEBFEJLESZTÉS</p>

                <a href="#">
                    <img class="img-rounded" src="images/tisztakonyha.png" alt="">
                </a>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>DINNERTIMETRAVEL.COM</h5>
                <p>WEBFEJLESZTÉS & MARKETING</p>

                <a href="#">
                    <img class="img-rounded" src="/images/dtt.png" alt="">
                </a>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>ODÚ BUDAPEST</h5>
                <p>WEBFEJLESZTÉS & MARKETING</p>
                <a href="#">
                    <img class="img-rounded" src="/images/odu.png" alt="">
                </a>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>BEMYGUEST</h5>
                <p>WEBFEJLESZTÉS & MARKETING</p>
                <a href="#">
                    <img class="img-rounded" src="/images/bemyguest.png" alt="">
                </a>

            </div>


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5>COLLETTIVO</h5>
                <p>WEBFEJLESZTÉS & MARKETING</p>
                <a href="#">
                    <img class="img-rounded"  src="/images/collettivo.png">
                </a>

            </div>

        </div>

    </div>
</div>



</section>
@endsection
<script>
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

</script>
<style>
    body{
        background: #FCFBFC;
        font-family: 'Open Sans',sans-serif;
        font-size: 20px;
    }

    .ion-minus{
        padding:0px 10px;
    }

    .portfolio{
        background-color:#f6f6f6;
        padding:60px 0px;
        font-size:13px;
        font-family: 'Raleway', sans-serif;
    }

    .portfolio .img-rounded {
        width: 100%;
        margin: 15px 0px;
    }

    .portfolio .img-rounded:hover {
        -webkit-transform: scale(1.20);
        -moz-transform: scale(1.20);
        -ms-transform: scale(1.20);
        -o-transform: scale(1.20);
        transform: scale(1.20);
        box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.2);
        opacity:0.7;
        -webkit-transition: all 1s ease-in-out;
        -moz-transition: all 1s ease-in-out;
        -o-transition: all 1s ease-in-out;
        transition: all 1s ease-in-out;
    }

</style>