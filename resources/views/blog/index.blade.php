@extends('main')

@section('title', '| Blog')

@section('content')


	<div class="row text-center">
		<h1>
			<a href="" class="typewrite section-title h1" data-period="2000" data-type='[ "BLOG" ]'>
				<span class="wrap"></span>

			</a>
		</h1>
		<p class="text-center w-75 m-auto">A kedvenc témáink.</p>

	</div>
	<hr>
	<!-- Blog card -->

	<div class="index-content">
		<div class="container">
			@foreach ($posts as $post)

			<a href="{{ route('blog.single', $post->slug) }}">
				<div class="col-lg-4">
					<div class="card">
						<img src="{{asset('images/' . $post->image)}}">
						<h4>{{ $post->title }}</h4>
                        <p>Közzétéve: {{ date('Y F j', strtotime($post->created_at)) }}</p>
                        <p>Kategória: {{ $post->category->name }}</p>
                        <div class="tags">
                            @foreach ($post->tags as $tag)
                                <p class="label label-default">{{ $tag->name }}</p>

                            @endforeach
                        </div>
						<p>{{ substr(strip_tags($post->body), 0, 250) }}{{ strlen(strip_tags($post->body)) > 250 ? '...' : "" }}</p>
						<a href="{{ route('blog.single', $post->slug) }}" class="blue-button animated pulse">Elolvasom</a>
					</div>
				</div>
			</a>
			@endforeach

		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                {!! $posts->links() !!}
            </div>
        </div>
    </div>

    </section>
@endsection
<script>
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script>