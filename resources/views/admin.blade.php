@extends('main')
@section('title','| Admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">ADMIN: Dashboard</div>

                    <div class="panel-body">You are logged in as <strong>ADMIN</strong>!</div>


                            {!! Form::open(['route'=>'admin.logout']) !!}

                            <br>
                            {{ Form::submit('Kijelentkezés', ['class' => 'btn btn-success btn-block']) }}

                            {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
@endsection