<!DOCTYPE html>
<html lang="hu">
  <head>
    @include('partials._head')
  </head>
  
  <body>

    @include('partials._nav')    

    <div class="container">
      @include('partials._messages')

      @yield('content')

      @include('partials._footer')

    </div> <!-- end of .container --> 

        @include('partials._javascript')

        @yield('scripts')
        @include('partials.cookies')
        @include('partials.scroll_up')
  </body>
</html>
